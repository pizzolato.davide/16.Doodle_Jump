﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _16.Doodle_Jump
{
    public partial class Gioco : Form
    {
        public Gioco()
        {
            InitializeComponent();
            inizializzaBarrette();
            fallY.Enabled = true;
        }

        PictureBox[] barretta = new PictureBox[30];
        long nBarrette = 0;
        int movimentoSuGiu = 0; //positivo = SU // negativo = GIU
        int movimentoX = 0;     //positivo = DX // negativo = SX
        Random rnd = new Random();
        int difficolta = 50; //Solo multipli di 5
        int flag = 0;
        int punteggio = 0;

        void inizializzaBarrette ()
        {
            for (int i = 0; i < 6; i++)
            {
                int x = 55 * i + 6;
                int y = 570;
                creaBarretta(x, y);
            }
            for (int y = 500; y >= 0; y=y-50)
            {
                creaBarretta(rnd.Next(6,282), y);
            }
        }

        void creaBarretta (int x, int y)
        {
            PictureBox newBarretta = new PictureBox();
            if(barretta[0] != null)
                barretta[0].Dispose();
            for (int i = 0; i < 29; i++)
                barretta[i] = barretta[i + 1];
            barretta[29] = newBarretta;
            newBarretta.BackColor = System.Drawing.Color.Transparent;
            newBarretta.Image = global::_16.Doodle_Jump.Properties.Resources.barretta;
            newBarretta.Location = new System.Drawing.Point(x, y);
            newBarretta.Name = "barretta"+nBarrette++;
            newBarretta.Size = new System.Drawing.Size(53, 15);
            newBarretta.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            newBarretta.TabIndex = 1;
            newBarretta.TabStop = false;
            this.Controls.Add(newBarretta);
        }

        private void fallY_Tick(object sender, EventArgs e)
        {
            lbl_punteggio.Text = Convert.ToString(punteggio);
            if (isPerso(personaggio.Location.Y))
            {
                fallY.Enabled = false;
                return;
            }
            int movimentoY;
            if (haToccatoBarretta(personaggio.Location.X, personaggio.Location.Y))
            {
                movimentoSuGiu = 45;
            }

            if (movimentoSuGiu > 0)
            {
                movimentoY = -5;
                movimentoSuGiu--;
            }
            else if(movimentoSuGiu < 0)
            {
                movimentoY = 5;
                movimentoSuGiu--;
            }
            else
            {
                movimentoY = 0;
                movimentoSuGiu--;
            }
            if (personaggio.Location.Y < 320 && movimentoSuGiu > 0)
            {
                punteggio += 10;
                if (barretta[29].Location.Y > 0)
                {
                    creaBarretta(rnd.Next(6, 282), barretta[29].Location.Y - difficolta);
                    flag++;
                    if (difficolta < 200 && flag == 20)
                    {
                        difficolta += 5;
                        flag = 0;
                    }
                }
                foreach (PictureBox pictBox in barretta)
                {
                    if (pictBox != null)
                    {
                        pictBox.Location = new System.Drawing.Point(pictBox.Location.X, pictBox.Location.Y - movimentoY);
                    }
                }
                personaggio.Location = new System.Drawing.Point(personaggio.Location.X + movimentoX, personaggio.Location.Y);
            }
            else
            {
                personaggio.Location = new System.Drawing.Point(personaggio.Location.X + movimentoX, personaggio.Location.Y + movimentoY);
            }
            if (personaggio.Location.X <= 0 - 23)
                personaggio.Location = new System.Drawing.Point(360 - 40, personaggio.Location.Y);
            else if (personaggio.Location.X >= 360 - 23)
                personaggio.Location = new System.Drawing.Point(0 - 20, personaggio.Location.Y);
            movimentoX = 0;
        }

        bool haToccatoBarretta (int x, int y)
        {
            y = y + 44;
            foreach (PictureBox barra in barretta)
            {
                if (barra != null)
                {
                    if ((y == barra.Location.Y) && (barra.Location.Y < 630))
                    {
                        if ((x > barra.Location.X - 40) && (x < barra.Location.X + 50) && (movimentoSuGiu <= 0) && (barra.Location.X > 3))
                        {
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        private void Gioco_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 110)
            {
                movimentoX = -10;
                personaggio.Image = global::_16.Doodle_Jump.Properties.Resources.carattereSX;
            }
            else if (e.KeyChar == 109)
            {
                movimentoX = 10;
                personaggio.Image = global::_16.Doodle_Jump.Properties.Resources.carattereDX;
            }
        }

        bool isPerso(int y)
        {
            if (y + 44 > 640)
            {
                schermataPerso();
                return true;
            }
            else
            {
                return false;
            }            
        }

        void schermataPerso()
        {
            GameOver gameOver = new GameOver(Convert.ToString(punteggio));
            gameOver.Show();
            this.Close();
        }
    }
}
