﻿namespace _16.Doodle_Jump
{
    partial class GameOver
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GameOver));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btt_playAgain = new System.Windows.Forms.PictureBox();
            this.btt_menu = new System.Windows.Forms.PictureBox();
            this.personaggio = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.animazioneJump = new System.Windows.Forms.Timer(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lbl_punteggio = new System.Windows.Forms.Label();
            this.lbl_punteggioRecord = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btt_playAgain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btt_menu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.personaggio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Image = global::_16.Doodle_Jump.Properties.Resources.gameOver;
            this.pictureBox1.Location = new System.Drawing.Point(12, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(320, 120);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // btt_playAgain
            // 
            this.btt_playAgain.BackColor = System.Drawing.Color.Transparent;
            this.btt_playAgain.Image = global::_16.Doodle_Jump.Properties.Resources.playagain;
            this.btt_playAgain.Location = new System.Drawing.Point(168, 285);
            this.btt_playAgain.Name = "btt_playAgain";
            this.btt_playAgain.Size = new System.Drawing.Size(106, 40);
            this.btt_playAgain.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btt_playAgain.TabIndex = 1;
            this.btt_playAgain.TabStop = false;
            this.btt_playAgain.Click += new System.EventHandler(this.btt_playAgain_Click);
            // 
            // btt_menu
            // 
            this.btt_menu.BackColor = System.Drawing.Color.Transparent;
            this.btt_menu.Image = global::_16.Doodle_Jump.Properties.Resources.menu;
            this.btt_menu.Location = new System.Drawing.Point(64, 360);
            this.btt_menu.Name = "btt_menu";
            this.btt_menu.Size = new System.Drawing.Size(106, 40);
            this.btt_menu.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btt_menu.TabIndex = 2;
            this.btt_menu.TabStop = false;
            this.btt_menu.Click += new System.EventHandler(this.btt_menu_Click);
            // 
            // personaggio
            // 
            this.personaggio.BackColor = System.Drawing.Color.Transparent;
            this.personaggio.Image = global::_16.Doodle_Jump.Properties.Resources.carattereDX;
            this.personaggio.Location = new System.Drawing.Point(249, 499);
            this.personaggio.Name = "personaggio";
            this.personaggio.Size = new System.Drawing.Size(46, 44);
            this.personaggio.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.personaggio.TabIndex = 5;
            this.personaggio.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox4.Image = global::_16.Doodle_Jump.Properties.Resources.barretta;
            this.pictureBox4.Location = new System.Drawing.Point(242, 544);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(53, 15);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox4.TabIndex = 4;
            this.pictureBox4.TabStop = false;
            // 
            // animazioneJump
            // 
            this.animazioneJump.Enabled = true;
            this.animazioneJump.Interval = 10;
            this.animazioneJump.Tick += new System.EventHandler(this.animazioneJump_Tick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(92, 157);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(88, 18);
            this.label1.TabIndex = 6;
            this.label1.Text = "Punteggio:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(32, 209);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(148, 18);
            this.label2.TabIndex = 7;
            this.label2.Text = "Punteggio Record:";
            // 
            // lbl_punteggio
            // 
            this.lbl_punteggio.BackColor = System.Drawing.Color.Transparent;
            this.lbl_punteggio.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_punteggio.Location = new System.Drawing.Point(186, 157);
            this.lbl_punteggio.Name = "lbl_punteggio";
            this.lbl_punteggio.Size = new System.Drawing.Size(88, 18);
            this.lbl_punteggio.TabIndex = 8;
            this.lbl_punteggio.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbl_punteggioRecord
            // 
            this.lbl_punteggioRecord.BackColor = System.Drawing.Color.Transparent;
            this.lbl_punteggioRecord.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_punteggioRecord.Location = new System.Drawing.Point(186, 209);
            this.lbl_punteggioRecord.Name = "lbl_punteggioRecord";
            this.lbl_punteggioRecord.Size = new System.Drawing.Size(88, 18);
            this.lbl_punteggioRecord.TabIndex = 9;
            this.lbl_punteggioRecord.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // GameOver
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::_16.Doodle_Jump.Properties.Resources.Sfondo;
            this.ClientSize = new System.Drawing.Size(344, 601);
            this.Controls.Add(this.lbl_punteggioRecord);
            this.Controls.Add(this.lbl_punteggio);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.personaggio);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.btt_menu);
            this.Controls.Add(this.btt_playAgain);
            this.Controls.Add(this.pictureBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "GameOver";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Doodle Jump";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btt_playAgain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btt_menu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.personaggio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox btt_playAgain;
        private System.Windows.Forms.PictureBox btt_menu;
        private System.Windows.Forms.PictureBox personaggio;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.Timer animazioneJump;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lbl_punteggio;
        private System.Windows.Forms.Label lbl_punteggioRecord;
    }
}