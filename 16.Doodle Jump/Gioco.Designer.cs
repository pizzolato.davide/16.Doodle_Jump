﻿namespace _16.Doodle_Jump
{
    partial class Gioco
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Gioco));
            this.fallY = new System.Windows.Forms.Timer(this.components);
            this.personaggio = new System.Windows.Forms.PictureBox();
            this.lbl_punteggio = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.personaggio)).BeginInit();
            this.SuspendLayout();
            // 
            // fallY
            // 
            this.fallY.Interval = 16;
            this.fallY.Tick += new System.EventHandler(this.fallY_Tick);
            // 
            // personaggio
            // 
            this.personaggio.BackColor = System.Drawing.Color.Transparent;
            this.personaggio.Image = global::_16.Doodle_Jump.Properties.Resources.carattereDX;
            this.personaggio.Location = new System.Drawing.Point(144, 356);
            this.personaggio.Name = "personaggio";
            this.personaggio.Size = new System.Drawing.Size(46, 44);
            this.personaggio.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.personaggio.TabIndex = 0;
            this.personaggio.TabStop = false;
            // 
            // lbl_punteggio
            // 
            this.lbl_punteggio.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_punteggio.AutoSize = true;
            this.lbl_punteggio.BackColor = System.Drawing.Color.Transparent;
            this.lbl_punteggio.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_punteggio.Location = new System.Drawing.Point(12, 9);
            this.lbl_punteggio.Name = "lbl_punteggio";
            this.lbl_punteggio.Size = new System.Drawing.Size(0, 18);
            this.lbl_punteggio.TabIndex = 10;
            this.lbl_punteggio.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Gioco
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(344, 601);
            this.Controls.Add(this.lbl_punteggio);
            this.Controls.Add(this.personaggio);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Name = "Gioco";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Doodle Jump";
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Gioco_KeyPress);
            ((System.ComponentModel.ISupportInitialize)(this.personaggio)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox personaggio;
        private System.Windows.Forms.Timer fallY;
        private System.Windows.Forms.Label lbl_punteggio;
    }
}