﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _16.Doodle_Jump
{
    public partial class Score : Form
    {
        public Score()
        {
            InitializeComponent();
            stampaPunteggi();
        }

        int movimentoSuGiu = 10; // pos = su // neg = giu 
        const int RECORD = 0;
        const int PUNTEGGIO_2 = 1;
        const int PUNTEGGIO_3 = 2;

        void stampaPunteggi()
        {
            string[] config = File.ReadAllLines(@"./config.txt");
            lbl_punteggio1.Text = config[RECORD];
            lbl_punteggio2.Text = config[PUNTEGGIO_2];
            lbl_punteggio3.Text = config[PUNTEGGIO_3];
        }

        private void btt_menu_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            int movimentoY;
            if (movimentoSuGiu > 0)
            {
                movimentoY = -2;
                movimentoSuGiu--;
            }
            else if (movimentoSuGiu < 0)
            {
                movimentoY = 2;
                movimentoSuGiu--;
            }
            else
            {
                movimentoY = 0;
                movimentoSuGiu--;
            }
            if (personaggio.Location.Y == 499)
            {
                movimentoSuGiu = 50;
            }
            personaggio.Location = new System.Drawing.Point(personaggio.Location.X, personaggio.Location.Y + movimentoY);

        }
    }
}
